package redis

import com.google.gson.Gson
import redis.clients.jedis.Jedis
import java.io.File
import java.io.InputStream

//KEYS *:Pizza*
//docker run --name redis_test -p 6379:6379 redis
fun main(args: Array<String>) {
//    val hostAndPort = hashSetOf<HostAndPort>(HostAndPort("localhost", 7000), HostAndPort("localhost", 7001), HostAndPort("localhost", 7002))
//    val jedis = JedisCluster(hostAndPort)
    val jedis = Jedis()
//    saveDataToDB(jedis)
    println("Pleas insert name you search for: ")
    val key = readLine()!!
    val keys = findShopKeys(jedis,key)
    val searchResults = keys.map{
            findPlaceOfKey(jedis, it)
        }
        .distinctBy {
            it.name
        }
            .sortedBy {
                it.name
            }

    for(i in 0..searchResults.size-1) {
        println("$i. ${searchResults.get(i).name}")
    }
    println("Insert number for all addresses:")
    val num = readLine()!!.toInt()
    println("You search for ${searchResults.get(num).name}")
    val places = findAllPlacesOfBusiness(searchResults.get(num).name, jedis)
    places.forEach{
        println(it)
    }
}

fun findAllPlacesOfBusiness(name: String, jedis: Jedis): List<String>{
    val keys = jedis.keys("*:$name")
    val places = mutableListOf<String>()
    keys.forEach{
        val businessV = Gson().fromJson(jedis.get(it), Business::class.java)
        places.add(businessV.address)
    }
    return places.toList()
}

fun findShopKeys(jedis: Jedis, name: String): Set<String> {
    return jedis.keys("*${name}*")
}

fun findPlaceOfKey(jedis: Jedis, key: String): Business {
    return Gson().fromJson(jedis.get(key), Business::class.java)
}

fun saveDataToDB(jedis: Jedis){
    val businessArr = readBusiness()
    businessArr.forEach{a-> jedis.set("business:${a.business_id}:${a.name}", Gson().toJson(a))}
}

fun readBusiness(): List<Business> {
    val inputStream: InputStream = File("business.json").inputStream()
    val inputString = inputStream.bufferedReader().use { it.readText() }.split("\n")
    val businessArr = inputString.map { str -> Gson().fromJson(str, Business::class.java)}
    return businessArr.filter { a -> a !== null }
}

