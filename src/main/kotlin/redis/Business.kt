package redis


class Business (
        var business_id: String,
        var name: String,
        var neighborhood: String,
        var address: String,
        var city: String,
        var state: String,
        var postal_code: String,
        var latitude: Number,
        var longitude: Number,
        var stars: Number,
        var review_count: Int,
        var is_open: Int,
        var attributes: Attributes,
        var categories: List<String>,
        var hours: Hours
)

class Attributes(
        var AcceptsInsurance: Boolean,
        var ByAppointmentOnly: Boolean,
        var BusinessAcceptsCreditCards: Boolean
)

class Hours (
        var Monday: String?,
        var Tuesday: String?,
        var Wendnesday: String?,
        var Thursday: String?,
        var Friday: String?,
        var Saturday: String?,
        var Sunday: String?
)