package mongo

import com.mongodb.BasicDBObject
import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

/**
 * docker run -p 27017:27017 --rm mongo:3.7
 *
 * mongoimport --db business --collection business --file business.json
 * mongoimport --db business --collection users  --file user.json
 * mongoimport --db business --collection reviews  --file review.json
 * ./import_short.sh
 */


fun main(args: Array<String>) {
    val mongoClient = MongoClient()
    val db = mongoClient.getDatabase("business")

    startAllTasks(db)
}

fun startAllTasks(db: MongoDatabase)  {
    var start = LocalDateTime.now()
    denormalizeDataBase(db)
    var end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized the database") //Needed 73773ms for denormalized the database

    start = LocalDateTime.now()
    deleteBefore2016Denomalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to delete all reviews before 2016") //Needed 88ms for denormalized to delete all reviews before 2016

    start = LocalDateTime.now()
    deleteBefore2016(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to delete all reviews before 2016") //Needed 8ms for normalized to delete all reviews before 2016

    start = LocalDateTime.now()
    findAllOfLasVegasDenormalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to find all in LA") //Needed 260ms for denormalized to find all in LA

    start = LocalDateTime.now()
    findAllOfLasVegas(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to find all in LA") //Needed 91ms for normalized to find all in LA

    start = LocalDateTime.now()
    sortCityByBusinessDenormalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to sort city with business") //Needed 15ms for denormalized to sort city with business

    start = LocalDateTime.now()
    sortCityByBusiness(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to sort city with business") //Needed 11ms for normalized to sort city with business

    start = LocalDateTime.now()
    top5LasVegasBurgerDenormalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to find top 5 burgers in LA") //Needed 2ms for denormalized to find top 5 burgers in LA

    start = LocalDateTime.now()
    top5LasVegasBurger(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to find top 5 burgers in LA") //Needed 0ms for normalized to find top 5 burgers in LA

    //denormalisiert fehlt
    start = LocalDateTime.now()
    countEachCategory(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to count each category") //Needed 0ms for normalized to count each category

    start = LocalDateTime.now()
    findAllReviewsBefore2017with5StarsDenomalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to load all 5 star reviews > 2017") //Needed 0ms for denormalized to load all 5 star reviews > 2017

    start = LocalDateTime.now()
    findAllReviewsBefore2017with5Stars(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to load all 5 star reviews > 2017") //Needed 0ms for normalized to load all 5 star reviews > 2017

    start = LocalDateTime.now()
    updateBurger(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to update burger") //Needed 9ms for denormalized to update burger

    start = LocalDateTime.now()
    updateBurgerDenormalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to update burger") //Needed 8ms for normalized to update burger

    start = LocalDateTime.now()
    updateStarsTo10Denomalized(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for denormalized to update stars") //Needed 99ms for denormalized to update stars

    start = LocalDateTime.now()
    updateStarsTo10(db)
    end = LocalDateTime.now()
    println("Needed ${ChronoUnit.MILLIS.between(start, end)}ms for normalized to update stars") //Needed 139ms for normalized to update stars
}

fun deleteBefore2016(db: MongoDatabase){
    val deleteQuery = BasicDBObject("date", BasicDBObject("\$lt", "2016-01-01")) //löschen dauerte 59096ms, 64337ms
    val reviewCollection = db.getCollection("reviews")
    reviewCollection.deleteMany(deleteQuery)
}

fun deleteBefore2016Denomalized(db: MongoDatabase){
    val reviewCollection = db.getCollection("businessDenormalized")
    val deleteQuery = BasicDBObject("\$pull",BasicDBObject("reviews", BasicDBObject("date", BasicDBObject("\$lt", "2016-01-01"))))
    reviewCollection.updateMany(BasicDBObject(), deleteQuery)
}

fun findAllOfLasVegas(db: MongoDatabase){
    val businessCollection = db.getCollection("business")
    val query = BasicDBObject("city", "Las Vegas")
    val results = businessCollection.find(query)
    results.map{
                it["name"] as String
            }
            .distinctBy {
                it
            }
            .sortedBy {
                it
            }
            .forEach{
//                println(it)
            }
}

fun findAllOfLasVegasDenormalized(db: MongoDatabase){
    val businessCollection = db.getCollection("businessDenormalized")
    val query = BasicDBObject("city", "Las Vegas")
    val results = businessCollection.find(query)
    results.map{
        it["name"] as String
    }
            .distinctBy {
                it
            }
            .sortedBy {
                it
            }
            .forEach{
//                println(it)
            }
}

fun sortCityByBusinessDenormalized(db: MongoDatabase){
    val businessCollection = db.getCollection("businessDenormalized")
    val innerGroupQuery = BasicDBObject("_id", "\$city")
    innerGroupQuery.put("count", BasicDBObject("\$sum", 1))
    val groupQuery = BasicDBObject("\$group", innerGroupQuery)
    val sortQuery = BasicDBObject("\$sort", BasicDBObject("count", -1))
    val results = businessCollection.aggregate(listOf(groupQuery, sortQuery))
    results
            .forEach{
//                println(it["_id"].toString() + " has " + it["count"].toString() + " different shops")
            }
}

fun sortCityByBusiness(db: MongoDatabase){
    val businessCollection = db.getCollection("business")
    val innerGroupQuery = BasicDBObject("_id", "\$city")
    innerGroupQuery.put("count", BasicDBObject("\$sum", 1))
    val groupQuery = BasicDBObject("\$group", innerGroupQuery)
    val sortQuery = BasicDBObject("\$sort", BasicDBObject("count", -1))
    val results = businessCollection.aggregate(listOf(groupQuery, sortQuery))
    results
            .forEach{
//                println(it["_id"].toString() + " has " + it["count"].toString() + " different shops")
            }
}

fun top5LasVegasBurger(db: MongoDatabase) {
    val businessCollection = db.getCollection("business")
    val query = BasicDBObject("\$match", BasicDBObject("city", "Las Vegas"))
    val burger = BasicDBObject("\$match", BasicDBObject("categories", "Burgers"))
    val grouping = BasicDBObject("_id", "\$business_id")
    grouping.append("business_name", "\$name")
    val group = BasicDBObject("\$project", grouping)

    val lookupReviews = BasicDBObject("from", "reviews")
    lookupReviews.append("localField", "_id")
    lookupReviews.append("foreignField", "business_id")
    lookupReviews.append("as", "review")
    val lookup = BasicDBObject("\$lookup", lookupReviews)

    val sortDate = BasicDBObject("\$sort", BasicDBObject("review.date", -1))

    val endStructure = BasicDBObject("text", "\$review.text")
    endStructure.append("_id", "\$business_name")
    val projectFinish = BasicDBObject("\$project", endStructure)

    val limit = BasicDBObject("\$limit", 5)

    val results = businessCollection.aggregate(listOf(query, burger, group, lookup, sortDate, limit,  projectFinish))

//    results.forEach{ println(it.toJson())}
}

fun top5LasVegasBurgerDenormalized(db: MongoDatabase) {
    val businessCollection = db.getCollection("businessDenormalized")
    val query = BasicDBObject("\$match", BasicDBObject("city", "Las Vegas"))
    val burger = BasicDBObject("\$match", BasicDBObject("categories", "Burgers"))

    val sortReviews = BasicDBObject("\$sort", BasicDBObject("reviews", -1))
    val sortDate = BasicDBObject("\$sort", BasicDBObject("reviews.0.date", -1))

    val endStructure = BasicDBObject("text", "\$reviews.text")
    endStructure.append("_id", "\$name")
    val projectFinish = BasicDBObject("\$project", endStructure)

    val limit = BasicDBObject("\$limit", 5)

    val results = businessCollection.aggregate(listOf(query, burger, sortReviews, sortDate, limit,  projectFinish))

//    results.forEach{ println(it.toJson())}
}

fun countEachCategory(db: MongoDatabase){
    val businessCollection = db.getCollection("business")

    val categoryQuery = BasicDBObject("\$unwind", "\$categories")

    val projectStructure = BasicDBObject("categories", "\$categories")
    projectStructure.append("_id", "\$business_id")
    val categoryProject = BasicDBObject("\$project", projectStructure)

    val lookupReviews = BasicDBObject("from", "reviews")
    lookupReviews.append("localField", "_id")
    lookupReviews.append("foreignField", "business_id")
    lookupReviews.append("as", "reviews")
    val lookup = BasicDBObject("\$lookup", lookupReviews)

    val projectFinalQuery = BasicDBObject("_id", "\$categories")
    projectFinalQuery.append("count", BasicDBObject("\$size", "\$reviews"))
    val projectVaginal = BasicDBObject("\$project", projectFinalQuery)

    val sorting = BasicDBObject("\$sort", BasicDBObject("count", -1))

    val results = businessCollection.aggregate(listOf(categoryQuery, categoryProject, lookup, projectVaginal, sorting))

//    results.forEach{ println(it.toJson())}
}

fun updateBurger(db: MongoDatabase){
    val businessCollection = db.getCollection("business")
    businessCollection.updateMany(BasicDBObject("categories", "Burgers"), BasicDBObject("\$set", BasicDBObject("categories.\$", "Burger")))
}

fun updateBurgerDenormalized(db: MongoDatabase){
    val businessCollection = db.getCollection("businessDenormalized")
    businessCollection.updateMany(BasicDBObject("categories", "Burgers"), BasicDBObject("\$set", BasicDBObject("categories.\$", "Burger")))
}

fun denormalizeDataBase(db: MongoDatabase) { //protocol:op_msg 1345ms

    val businessCollection = db.getCollection("business")

    val lookupQuery = BasicDBObject("from", "reviews")
    lookupQuery.append("localField", "business_id")
    lookupQuery.append("foreignField", "business_id")
    lookupQuery.append("as", "reviews")
    val lookUpForReview = BasicDBObject("\$lookup", lookupQuery)

    val lookUpForUserQuery = BasicDBObject("from", "users")
    lookUpForUserQuery.append("localField", "reviews.user_id")
    lookUpForUserQuery.append("foreignField", "user_id")
    lookUpForUserQuery.append("as", "users")
    val lookUpForUser = BasicDBObject("\$lookup", lookUpForUserQuery)

    val result = businessCollection.aggregate(listOf(lookUpForReview, lookUpForUser))

    db.createCollection("businessDenormalized")
    val businessDenormalized = db.getCollection("businessDenormalized")
    businessDenormalized.insertMany(result.toMutableList())
}

fun updateStarsTo10Denomalized(db: MongoDatabase) {
    val businessCollection = db.getCollection("businessDenormalized")

    val doubleStars = BasicDBObject("\$mul", BasicDBObject("stars", 2))
    val doubleUsersAverageStars = BasicDBObject("\$mul", BasicDBObject("users.\$[].average_stars", 2))
    val doubleReviewsStars = BasicDBObject("\$mul", BasicDBObject("reviews.\$[].stars", 2))

    businessCollection.updateMany(BasicDBObject(), doubleStars)
    businessCollection.updateMany(BasicDBObject("users", BasicDBObject("\$not", BasicDBObject("\$size", 0))), doubleUsersAverageStars)
    businessCollection.updateMany(BasicDBObject("reviews", BasicDBObject("\$not", BasicDBObject("\$size", 0))), doubleReviewsStars)
}

fun updateStarsTo10(db: MongoDatabase) {
    val businessCollection = db.getCollection("business")
    val usersCollection = db.getCollection("users")
    val reviewsCollection = db.getCollection("reviews")

    val doubleStarsBusiness = BasicDBObject("\$mul", BasicDBObject("stars", 2))
    val doubleStarsReviews = BasicDBObject("\$mul", BasicDBObject("stars", 2))
    val doubleStarsUsers = BasicDBObject("\$mul", BasicDBObject("stars", 2))

    businessCollection.updateMany(BasicDBObject(), doubleStarsBusiness)
    usersCollection.updateMany(BasicDBObject(), doubleStarsUsers)
    reviewsCollection.updateMany(BasicDBObject(), doubleStarsReviews)
}

fun findAllReviewsBefore2017with5Stars(db: MongoDatabase) {
    val reviewsCollection = db.getCollection("reviews")

    val searchQuery = BasicDBObject("stars", 5)
    searchQuery.append("date", BasicDBObject("\$gt","2017-01-01"))

    val result = reviewsCollection.find(searchQuery)

//    result.forEach { println(it.toJson()) }
}

fun findAllReviewsBefore2017with5StarsDenomalized(db: MongoDatabase) {
    val reviewsCollection = db.getCollection("businessDenormalized")

    val removeEmtpyArrayAndSetPath = BasicDBObject("path", "\$reviews")
    removeEmtpyArrayAndSetPath.append("preserveNullAndEmptyArrays", false)
    val unwind = BasicDBObject("\$unwind", removeEmtpyArrayAndSetPath)

    val filterStars = BasicDBObject("reviews.stars", 5)
    val filsterDate = BasicDBObject("reviews.date", BasicDBObject("\$gt","2017-01-01"))
    val match = BasicDBObject("\$match", BasicDBObject("\$and", listOf(filterStars, filsterDate)))

    val result = reviewsCollection.aggregate(listOf(unwind, match))

//    result.forEach { println(it.toJson()) }
}